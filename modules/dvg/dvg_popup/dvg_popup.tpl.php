<div class="popup-overlay"></div>
<div class="popup-content">
  <h2><?php print $title; ?></h2>
  <button class="close close-x">&times;</button>
  <div class="body">
    <?php print $body; ?>
  </div>
  <button class="close close-button"><?php print $button ?></button>
</div>
