Introduction
============
This module adds a fieldset around multiple form items (radios and checkboxes).

This is an accessibility improvement, as explained here (in Dutch):
https://www.accessibility.nl/kennisbank/artikelen/toegankelijke-formulieren-in-webrichtlijnen-2
