<?php
/**
 * @file
 * dvg_piwik.dvg.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dvg_piwik_dvg_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer piwik'.
  $permissions['administer matomo'] = array(
    'name' => 'administer matomo',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'matomo',
  );

  // Exported permission: 'opt-in or out of tracking'.
  $permissions['opt-in or out of matomo tracking'] = array(
    'name' => 'opt-in or out of matomo tracking',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'matomo',
  );

  // Exported permission: 'use PHP for tracking visibility'.
  $permissions['use php for matomo tracking visibility'] = array(
    'name' => 'use php for matomo tracking visibility',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'matomo',
  );

  return $permissions;
}
