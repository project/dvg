/* $Id:

Description
===========
After a given timeout has passed, DiGID users are logged out. They can reset the timeout, logout, or ignore it in which case they'll be logged out after the time has elapsed. This is all backed up by a server side logout if js is disable or bypassed.

Features
========
* Configurable messaging.
* Configurable redirect url, with the destination automatically appended.
* Configurable timeout for DiGID User.
* Configurable maximum session timeout for DiGID User.
