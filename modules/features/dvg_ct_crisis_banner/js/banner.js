(function ($) {

  function a11yClick(event) {
    if (event.type === 'click') {
      return true;
    }
    else if (event.type === 'keypress') {
      var code = event.charCode || event.keyCode;
      if ((code === 32) || (code === 13)) {
        return true;
      }
    } else {
      return false;
    }
  }

  Drupal.behaviors.dvgCrisisBanner = {
    attach: function (context, settings) {
      if (Drupal.settings.dvgCrisisBanner && Drupal.settings.dvgCrisisBanner.code) {
        var localstorageKey = 'banner-' + Drupal.settings.dvgCrisisBanner.code;
        var $node = $('#node-' + Drupal.settings.dvgCrisisBanner.nid, context);
        var $closeBtn = $node.find('.banner-close-button');
        var $cookieEnabled = !!navigator.cookieEnabled;

        // Adding a cookie check because Firefox throws a security error when
        // cookies are disabled and localStorage is called.
        if ($cookieEnabled) {
          if ($node.length) {
            var closed = localStorage.getItem(localstorageKey);
            if (typeof closed === 'undefined' || closed == null) {
              $closeBtn.once('dvgCrisisBanner').on('click keypress', function(e){
                if(a11yClick(e) === true) {
                  e.preventDefault();
                  closeNodeAction(localstorageKey);
                  $('body').removeClass('with-crisis-banner');
                  $node.hide();
                }
              });
            }
            else {
              $('body').removeClass('with-crisis-banner');
              $node.hide()
            }
          }
        } else {
          $closeBtn.once('dvgCrisisBanner').on('click keypress', function(e){
            if(a11yClick(e) === true) {
              e.preventDefault();
              $('body').removeClass('with-crisis-banner');
              $node.hide();
            }
          });
        }
      }

      function closeNodeAction(localstorageKey) {
        localStorage.setItem(localstorageKey, new Date().getTime());
      }
    }
  };
}(jQuery));
