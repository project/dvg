<?php

/**
 * @file
 * Hooks provided by the DVG Global API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add options to testdata form.
 *
 * Modules may implement this hook to add items to the testdata form.
 *
 * @return array
 *  An associative array containing fields to be added to the testdata form.
 *
 */
function hook_dvg_global_testdata() {

  $form = array();
  $form['stuf_bg'] = array(
    '#type' => 'fieldset',
    '#title' => t('StUF-BG'),
  );
  $form['stuf_bg']['dvg_stuf_bg_debug_bsn'] = array(
    '#type' => 'textfield',
    '#title' => t('Debug BSN'),
    '#default_value' => variable_get('dvg_stuf_bg_debug_bsn', ''),
    '#description' => t('Debug BSN used when debug mode is on.'),
  );
  return $form;
}

/**
 * Add status messages to the DvG status report page.
 *
 * Use this hook to add DvG related status messages. Which are grouped together
 * on the DvG status report.
 *
 * @return array
 *   An associative array where the keys are arbitrary but must be unique (it
 *   is suggested to use the module short name as a prefix) and the values are
 *   themselves associative arrays with the following elements:
 *   - title: The name of the requirement.
 *   - value: The current value (e.g., version, time, level, etc). During
 *     install phase, this should only be used for version numbers, do not set
 *     it if not applicable.
 *   - description: The description of the requirement/status.
 *   - severity: The requirement's result/severity level, one of:
 *     - REQUIREMENT_INFO: For info only.
 *     - REQUIREMENT_OK: The requirement is satisfied.
 *     - REQUIREMENT_WARNING: The requirement failed with a warning.
 *     - REQUIREMENT_ERROR: The requirement failed with an error.
 *
 * @see hook_requirements().
 */
function hook_dvg_requirements() {
  $requirements = array();

  // Caching.
  $cache_description = l(t('Caching configuration'), 'admin/config/development/performance');
  $requirements['dvg_global_cache'] = array(
    'title' => t('Cache pages'),
    'description' => variable_get('cache') ? '' : $cache_description,
    'value' => variable_get('cache') ? t('Enabled') : t('Disabled'),
    'severity' => REQUIREMENT_INFO,
  );
  $requirements['dvg_global_block_cache'] = array(
    'title' => t('Cache blocks'),
    'description' => variable_get('block_cache') ? '' : $cache_description,
    'value' => variable_get('block_cache') ? t('Enabled') : t('Disabled'),
    'severity' => REQUIREMENT_INFO,
  );

  return $requirements;
}

/**
 * Alter the DvG requirements.
 *
 * Allow modules to alter the requirements shown on the DvG Status page.
 *
 * @param array $requirements
 *   The list of requirements.
 */
function hook_dvg_requirements_alter(&$requirements) {
  // Change the severity from INFO to OK.
  $requirements['dvg_global_cache']['severity'] = REQUIREMENT_OK;

  // Add an extra status.
  $requirements['dvg_global_cache_extra'] = array(
    'title' => t('Extra status'),
    'description' => t('A description.'),
    'severity' => REQUIREMENT_INFO,
  );
}

/**
 * @} End of "addtogroup hooks".
 */
