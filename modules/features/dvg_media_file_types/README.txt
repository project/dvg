DvG Media File Types
--------------------

Used variables:
---------------

dvg_open_files_new_tab : Boolean if file links should be opened in a new tab using
                         target="_blank" (default) or in the same tab/window. (drush vset)
