api = 2
core = 7.x
projects[drupal][version] = 7.98

; Patches for Core
projects[drupal][patch][] = "https://www.drupal.org/files/pager_ellipsis-470428-5.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/issues/default_page_headers-2457613-5.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-bootstrap_theme_init_issue-2086335-34.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/issues/w3c_search_block_validation-D7-2637680-44.patch"
